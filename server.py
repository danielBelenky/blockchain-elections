#!/bin/env python3

import json

import web3

from web3 import Web3
from solc import compile_standard
import sys
from flask import Flask, render_template, request, redirect
import logging
from time import time

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

app = Flask(__name__)
w3 = None
w3_contract = None
start_time = 0
TIMEOUT = 60 * 5


def init_web3(contract_address=None):
    #contract_source = load_contract('Election')
    provider = Web3.HTTPProvider('http://localhost:7545')
    # compiled_contract = compile_contract('Election.sol', contract_source)
    w3 = Web3(provider)
    with open('./build/contracts/Election.json', 'r') as cf:
        contract_json = json.load(cf)
    contract_address = contract_json['networks']['5777']['address']
    contract_abi = contract_json['abi']
    contract = w3.eth.contract(address=contract_address, abi=contract_abi)
    w3.eth.defaultAccount = w3.eth.accounts[0]
    return w3, contract


def call_method(method_name, *params):
    '''Call a method and wait for the transaction to finish
    '''
    method = getattr(w3_contract.functions, method_name)
    tx_hash = method(*params).transact({'from': w3.eth.defaultAccount})
    return w3.eth.waitForTransactionReceipt(tx_hash)


def deploy_contract(w3, compiled_contract):
    # set pre-funded account as sender
    w3.eth.defaultAccount = w3.eth.accounts[0]
    # get bytecode
    bytecode = compiled_contract['contracts']['Election.sol']['Election']['evm']['bytecode']['object']

    # get abi
    abi = json.loads(compiled_contract['contracts']['Election.sol']
            ['Election']['metadata'])['output']['abi']

    contract = w3.eth.contract(abi=abi, bytecode=bytecode)

    # Submit the transaction that deploys the contract
    tx_hash = contract.constructor().transact()

    # Wait for the transaction to be mined, and get the transaction receipt
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

    contract = w3.eth.contract(
            address=tx_receipt.contractAddress,
            abi=abi
            )
    return contract


def compile_contract(contract_name, contract_source):
    return compile_standard({
        "language": "Solidity",
        "sources": {
            contract_name: {
                "content": contract_source
                }
            },
        "settings":
        {
            "outputSelection": {
                "*": {
                    "*": [
                        "metadata", "evm.bytecode", "evm.bytecode.sourceMap"
                        ]
                    }
                }
            }
        })


    def load_contract(contract_name):
        with open('contracts/' + contract_name + '.sol', 'r') as contract:
            return contract.read()


@app.route('/new_candidate/', methods=['GET'])
def new_candidate():
    candidate_name = request.args.get('candidate_name')
    call_method('addCandidate', candidate_name)
    return redirect('/register')


@app.route('/register')
def register():
    if int(time()) - start_time >= TIMEOUT:
        return redirect('/vote')
    candidates = (
        w3_contract.caller.candidates(c)[1]
        for c in range(1, w3_contract.caller.candidatesCount()+1)
    )
    elapsed_time = int(time()) - start_time
    return render_template(
        'registration.html',
        account_id=w3.eth.defaultAccount,
        candidates=candidates,
        timeout_duration_sec=(TIMEOUT -  elapsed_time)
    )


@app.route('/vote')
def vote():
    if not time_to_vote(): return redirect('/register')
    candidates = (
        w3_contract.caller.candidates(c)[1]
        for c in range(1, w3_contract.caller.candidatesCount()+1)
    )
    return render_template(
        'vote.html',
        account_id=w3.eth.defaultAccount,
        candidates=candidates,
        time_to_vote=time_to_vote()
    )


def time_to_vote():
    elapsed_time = int(time()) - start_time
    return (TIMEOUT -  elapsed_time) <= 0


def init():
    global w3
    global w3_contract
    global start_time
    w3, w3_contract = init_web3()
    start_time = int(time())


if __name__ == '__main__':
    init()
    app.run('0.0.0.0', '5720', debug=True)
